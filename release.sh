APP_NAME="$1"
TYPE="$2"
SCOPE="$3"

if [ -z "$SCOPE" ]; then
  SCOPE="auto"
fi

echo "Using scope $SCOPE"
echo "Using build type $TYPE"

# We get the next version, without tagging
echo "Getting next version"
nextversion="$(source semtag $TYPE -f -o -s $SCOPE)"

if [[ $nextversion == *"ERROR"* ]]; then
  echo "version could not be obtained: ${nextversion}"
  exit 1
fi

DOCKER_TAG="${DOCKER_REGISTRY}${APP_NAME}:${nextversion}"
echo "Tagging: ${DOCKER_TAG}"

docker tag $APP_NAME:local $DOCKER_TAG
docker push $DOCKER_TAG

# We update the tag with the new version
./semtag $TYPE -f -v $nextversion

