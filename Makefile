.PHONY: *

NAME := topic-runner

HELP_TAB_WIDTH = 30
.DEFAULT_GOAL := help

SHELL=/bin/sh -o pipefail

RELEASE_SCOPE ?= patch

ifdef CI_COMMIT_REF_NAME
MAIN_BRANCH := $(findstring main,$(CI_COMMIT_REF_NAME))$(findstring master,$(CI_COMMIT_REF_NAME))
RELEASE_TYPE := $(if $(MAIN_BRANCH),final,alpha)
NEXT_VERSION := $(shell ./semtag $(RELEASE_TYPE) -o -s $(RELEASE_SCOPE) $(if $(MAIN_BRANCH),-f,))
else
BRANCH := $(shell git branch | grep \* | cut -d ' ' -f2)
MAIN_BRANCH := $(findstring main,$(BRANCH))$(findstring master,$(BRANCH))
RELEASE_TYPE := $(if $(MAIN_BRANCH),final,alpha)
NEXT_VERSION := $(shell ./semtag $(RELEASE_TYPE) -o -s $(RELEASE_SCOPE))
endif
VERSION_CHECK := $(if $(findstring ERROR,$(NEXT_VERSION)),$(error $(NEXT_VERSION)),valid)
JAR_NAME := $(NAME)-$(NEXT_VERSION).jar

check-dependency = $(if $(shell command -v $(1)),,$(error Make sure $(1) is installed))

get-version:
	@echo $(NEXT_VERSION)

print:
	$(foreach var,$(.VARIABLES),$(info $(var) = $($(var))))

help:
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

initialize:
	@mkdir -p ./.build

clean:
	@rm -rf ./.build

release:
	@./semtag $(RELEASE_TYPE) -f -v $(NEXT_VERSION)

DOCKER_NAME := $(NAME):$(NEXT_VERSION)
DOCKER_URI := $(DOCKER_REGISTRY)$(DOCKER_NAME)

check-docker-dependencies:
	@$(call check-dependency,docker)

build: check-docker-dependencies
	@echo "Building $(NEXT_VERSION)"
	@docker build -t $(DOCKER_NAME) .

push: build
	@docker tag $(DOCKER_NAME) $(DOCKER_URI)
	@docker push $(DOCKER_URI)