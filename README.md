# topic-runner
Dockerized app that will add Kafka topics or update their allowed configurations based on configurations in `topic-def.yml`

## Topic definition file
Topics are defined in `topic-def.yml`. The top level is the topic name, partitions and replication are required. Example:

```
transactions.clo.rejected.results:
  partitions: 1
  replication: 2
  config:
    cleanup.policy: delete
    retention.bytes: 1099511627776 # 1TB
```

## Replication environment override
Often environments will contain different numbers of kafka brokers. Local test environments will likely just contain a
single broker. As a result, testing locally will likely fail for any topics with `replication` set to more than one
(**replication factor must be <= #brokers**). In order to override the configured replication factor for
_all_ topics, set the `MAX_TOPIC_REPLICATION_FACTOR` env var.

## Usage
By default the hostname `kafka` and default port `9092` is used to connect to Kafka; the environment variable
BOOTSTRAP_SERVERS can be used to override.  See the `Dockerfile` and `topic-runner.sh` for implementation.

Example docker run
`docker run --rm -e MAX_TOPIC_REPLICATION_FACTOR=1 mdvdregsrvprd01.idine.com:5000/topic-runner:0.1.0`

## Deployment
The docker container is built and published to the on-prem docker repository automatically whenever changes are merged
to the `main` branch, via a [TeamCity job](https://teamcity.idine.com/viewType.html?buildTypeId=PlatformTeam_Utils_topic-runner_1ReleaseBuild).
To actually deploy topic-runner, you'll need to run the TeamCity job for the appropriate environment; this will run the
container and create/modify topics.
- [The dev job](https://teamcity.idine.com/viewType.html?buildTypeId=Deployments_Docker_Dev_topic-runnerMsk) applies to the
  single on-prem test environment, and AWS dev. Both authenticated and un-authenticated MSK will be affected.
- [The staging job](https://teamcity.idine.com/viewType.html?buildTypeId=Deployments_Docker_topic-runner_StagingDeployMskStgMskAuthStg)
  applies only to AWS staging. Both authenticated and un-authenticated MSK will be affected.
- [The production job](https://teamcity.idine.com/viewType.html?buildTypeId=Deployments_Docker_topic-runner_ProdDeployMskProdMskAuthProdOnPremProd)
  applies to the on-prem prod environment and AWS prod. Both authenticated and un-authenticated MSK will be affected.

## Local Testing
Make desired changes to topic-def.yml
Run `docker-compose up -d`
Validate using kcat
 ex: `kcat -b localhost:9092 -L`
