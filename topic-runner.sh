#!/usr/bin/env bash

if [[ -z $KAFKA_USERNAME || -z $KAFKA_PASSWORD ]]; then
  echo running with unauthenticated kafka
  bin/kafka-configurator --file topic-def.yml --bootstrap-servers $BOOTSTRAP_SERVERS
else
  echo running with authenticated kafka
(
  cat <<HERE
ssl.endpoint.identification.algorithm=https
sasl.mechanism=SCRAM-SHA-512
request.timeout.ms=20000
retry.backoff.ms=500
sasl.jaas.config=org.apache.kafka.common.security.scram.ScramLoginModule required username="$KAFKA_USERNAME" password="$KAFKA_PASSWORD";
security.protocol=SASL_SSL

HERE
) > /tmp/admin.properties
  bin/kafka-configurator --file topic-def.yml --admin-properties-file /tmp/admin.properties --bootstrap-servers $BOOTSTRAP_SERVERS
fi

