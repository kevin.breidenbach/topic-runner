FROM skyuk/kafka-configurator:0.14.0

ADD topic-def.yml /opt/docker/topic-def.yml
ADD topic-runner.sh /opt/docker/topic-runner.sh

ENV BOOTSTRAP_SERVERS "kafka:9092"
ENV KAFKA_USERNAME ""
ENV KAFKA_PASSWORD ""

ENTRYPOINT ["/opt/docker/topic-runner.sh"]
